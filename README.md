# PadSquad - Man Test (by Israel Senior)

> Magnifying glass FX Scroller <sub><sup>[Reference](https://padsquad.celtra.com/preview/32d98755)</sup></sub> GWD component for [PadSquad](https://padsquad.com/).

## 🗒 Table of contents
1. [Features](#features)
2. [Tech Stack](#tech-stack)
3. [Getting started](#getting-started)
3.1. [For GWD](#for-gwd)
3.2. [For web](#for-web)
4. [Example](#example)
5. [Parameters](#parameters)
6. [Support](#support)

## ✨ Features

- Responsive component
- Multiple parameters
- Ready for GWD Custom Component

## 🛠 Tech Stack

**Client:** [vue@2.6.14](https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.14/vue.min.js), [core-js@3.21.0](https://cdnjs.cloudflare.com/ajax/libs/core-js/3.21.0/minified.min.js), [gsap@3.9.1](https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/gsap.min.js), [@gsap/draggable@3.9.1](https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/Draggable.min.js), [@gsap/scrollTrigger@3.9.1](https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/ScrollTrigger.min.js)

**Tools:** [GWD@14.0.4](https://webdesigner.withgoogle.com/)

## 🚀 Getting started
A component is available mainly for GWD although it can be used for web as well. The following is the documentation and the step-by-step to use it in different environments. There is also a base index.html file to facilitate its edition on the web.

### For GWD

1. Clone repository `git clone`
2. Go to folder `cd padsquad-man-test`
3. ZIP the fields `bash compress.sh` <sub><sup>**Code tested on MacOS**</sup></sub>. Get the zip file in the `dist/` folder 

🚨 **NOTE:** In case it does not work you can run the compression of the files inside `src/`. Please consider that you should NOT compress the folder but the files inside it to be able to be exported properly in GWD.

### For web

Same steps as with GWD without point 3. In this case, the files can be tested with the `index.html` file in the repository root.

Inside you can find the custom component tag `<magnifying-glass />` and a reference with the viable parameters for the component, which are detailed later in this document.

## 📌 Example

By placing the following code you can use the component on the web, the parameters are detailed below.

❗ **ctaImg** variable in `/src/mg-index.js` must be manually placed depending on whether it goes into production or development

```html
<div id="ad-canvas">
  <magnifying-glass
    mg-image="./assets/img/veronica_mars_poster.jpg"
    mg-zoom="3.04"
    mg-color="#fff"
    mg-line-width="30"
    mg-radius="225"
    mg-position-x="50"
    mg-position-y="50"
    mg-cta-url="https://padsquad.com/"
  />
</div>
```
## 👨🏽‍💻 Parameters

| Key | Type | Default | Description |
| :-- | :--- | :------ | :---------- |
| mg-image **<sup>*</sup>** | String | `null` | Imagen de portada **required** |
| mg-zoom | Float | `0 <` | Magnifier zoom |
| mg-color | String | `#ffffff` | Magnifier frame color **# is required** |
| mg-line-width | Number | `30 <` | Magnifier frame width |
| mg-radius | Number | `200 <` | Magnifier radius |
| mg-position-x | Number | `50` | X-axis position as a function of the container **0-100%** |
| mg-position-y | Number | `50` | Y-axis position as a function of the container **0-100%** |
| mg-cta-url | String | `#` | URL for CTA href |


## 🤖 Support

For support, email israsenior.dev@gmail.com