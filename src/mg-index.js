window.addEventListener("DOMContentLoaded", (event) => {
  const componentElement = document.querySelector("magnifying-glass");
  const rootElement = document.createElement("div");

  rootElement.setAttribute("id", "mg-app");
  componentElement.appendChild(rootElement);

  // GWD Attributes
  const mgImage = componentElement.getAttribute("mg-image");
  const mgZoom = componentElement.getAttribute("mg-zoom") || 2.04;
  const mgColor = componentElement.getAttribute("mg-color") || "#FFF";
  const mgLineWidth = componentElement.getAttribute("mg-line-width") || 30;
  const mgRadius = componentElement.getAttribute("mg-radius") || 150;
  const mgPositionX = componentElement.getAttribute("mg-position-x") || 50;
  const mgPositionY = componentElement.getAttribute("mg-position-y") || 50;

  const mgCtaUrl = componentElement.getAttribute("mg-cta-url") || "#";
  // const mgCtaImg = componentElement.getAttribute("mg-cta-img");
  // const mgCtaTxt = componentElement.getAttribute("mg-cta-text");
  
  // Magnifie component vue
  const Magnifier = Vue.component("Magnifier", {
    template: `
      <svg class="magnifier" :viewBox="viewBox">
        <circle
          ref="magnifier"
          cx="0"
          cy="0"
          :r="radius * 1.4"
          :transform="transform"
        />
      </svg>
    `,
    props: ["position", "radius", "container"],
    computed: {
      viewBox() {
        return `0 0 ${this.container.width} ${this.container.height}`;
      },
      transform() {
        //  This transform overwrites the transform that gsap sets
        return `translate(${this.position.x}, ${this.position.y})`;
      },
    },
    mounted() {
      this.createDraggable();
    },
    methods: {
      createDraggable: function () {
        const element = this.$refs.magnifier;
        const self = this;
        Draggable.create(element, {
          type: "x, y",
          onDrag() {
            self.$emit("update", {
              x: this.deltaX,
              y: this.deltaY,
            });
          },
        });
      },
    },
  });

  // Init Data state json
  let state = {
    ctx: null,
    ctx2: null,
    magnification: mgZoom,
    position: {
      x: mgPositionX,
      y: mgPositionY,
    },
    container: {
      width: 0,
      height: 0,
    },
    radius: mgRadius,
    cta: mgCtaUrl
    // mgCtaImg
  };

  // Vue app
  new Vue({
    el: "#mg-app",
    data() {
      return state;
    },
    watch: {
      position: {
        handler() {
          this.drawMagnifier();
        },
        deep: true,
      },
    },
    components: {
      Magnifier
    },
    template: `
      <div id="mg-component">
        <div id="mg-container" class="container">

            <div id="mg-container-canvas">
              <canvas id="canvas-original"></canvas>
              <canvas id="canvas-magnification"></canvas>
              <Magnifier :position="position" :radius="radius" :container="container" @update="updateMagnifier" />
            </div>

            <div id="mg-container-cta">
              <a id="mg-cta" :href="cta" target="_blank" rel="noopener noreferrer">
                Add to calendar
              </a>
            </div>

        </div>
      </div>
    `,
    mounted() {
      // Canvas define
      this.ctx = document.getElementById("canvas-original").getContext("2d");
      this.ctx2 = document
        .getElementById("canvas-magnification")
        .getContext("2d");

      // Create image
      let image = new Image();

      image.addEventListener("load", () => {
        this.initCanvasses(image);
        this.positionMagnifier();
      });
      image.src = mgImage;
    },
    methods: {
      // Config both canvas
      initCanvasses: function (image) {
        let w = image.naturalWidth,
          h = image.naturalHeight;

        // Canvas original iamge
        this.ctx.canvas.width = w;
        this.ctx.canvas.height = h;

        // Canvas for magnifier
        this.ctx2.canvas.width = w;
        this.ctx2.canvas.height = h;
        this.ctx2.lineWidth = mgLineWidth;
        this.ctx2.strokeStyle = mgColor;

        // Matching container with image
        this.container.width = w;
        this.container.height = h;

        // Drawing image to canvas original
        this.ctx.drawImage(image, 0, 0);
      },
      // Init magnifier position
      positionMagnifier() {
        this.position.x = (this.position.x * this.ctx.canvas.width) / 100;
        this.position.y = (this.position.y * this.ctx.canvas.height) / 100;
      },
      // Update magnifier position with watcher
      updateMagnifier(delta) {
        this.position.x += delta.x;
        this.position.y += delta.y;
      },
      // Drawing Magnifier
      drawMagnifier() {
        // Clean canvas trails
        this.ctx2.clearRect(
          0,
          0,
          this.ctx2.canvas.width,
          this.ctx2.canvas.height
        );

        // Create clipping mask
        this.ctx2.save();
        this.ctx2.beginPath();
        this.ctx2.arc(
          this.position.x,
          this.position.y,
          this.radius,
          0,
          2 * Math.PI
        );
        this.ctx2.stroke();
        this.ctx2.clip();

        // Draw magnified
        let size = this.radius * 2;
        let r = size / this.magnification;

        this.ctx2.drawImage(
          this.ctx.canvas,
          this.position.x - r / 2,
          this.position.y - r / 2,
          r,
          r,
          this.position.x - this.radius,
          this.position.y - this.radius,
          2 * this.radius,
          2 * this.radius
        );

        // Undo clipping
        this.ctx2.restore();
      },
    },
  });

  
  const canvasElement = document.getElementById("canvas-original");
  const containerElement = document.getElementById("mg-container");

  // Adding filter to canvas
  // TODO: this could be a param
  canvasElement.style.filter = "grayScale(1)";

  // Setting Scroll
  gsap.to("#mg-container", {
    scrollTrigger: {
      trigger: "#mg-component",
      start: "center center",
      end: () => "+=" + canvasElement.offsetHeight,
      scrub: true,
      toggleActions: "restart pause reverse pause"
    },
    y: 200,
    duration: 3
  })
});

